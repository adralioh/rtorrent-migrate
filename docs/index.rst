rtorrent-migrate documentation
==============================
rtorrent-migrate is a command-line script used to bulk-convert your data and/or session directory of torrents in rTorrent

Also includes a :class:`class <rtorrent_migrate.RTorrentMigrate>` for use in Python scripts

Source and install instructions are available on `GitLab <https://gitlab.com/adralioh/rtorrent-migrate>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage <command-line>
   examples <examples>
   class
