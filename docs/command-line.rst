command-line usage
==================

.. argparse::
   :module: rtorrent_migrate.migrator
   :func: _get_parser
   :prog: rtorrent-migrate
