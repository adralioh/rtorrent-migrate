command-line examples
=====================

.. highlight:: shell

..
   line break substitution var
.. |br| raw:: html

   <br/>

* change the data dir from `/torrents` to `/nas/torrents` for all rtorrent files in the dir `.rtorrent`::

    rtorrent-migrate --data /torrents /nas/torrents .rtorrent

* change the session dir from `/torrents/.session` to `/nas/.rtorrent` for all rtorrent files in the dir `.rtorrent`::

    rtorrent-migrate --session /torrents/.session /nas/.rtorrent .rtorrent

* change both the data and session dirs in one command::

    rtorrent-migrate \
      -d /torrents /nas/torrents \
      -s /torrents/.session /nas/.rtorrent \
      .rtorrent

* change all files in `.rtorrent` that end with `.rtorrent` |br|
  by default, it will match files that end with `.torrent.rtorrent`::

    rtorrent-migrate -r '.+\.rtorrent' -d /torrents /nas/torrents .rtorrent

* you can also specify files directly instead of a directory::

    rtorrent-migrate \
      -d /torrents /nas/torrents \
      torrent1.torrent.rtorrent torrent2.torrent.rtorrent

* ignore any errors encountered when reading/writing files::

    rtorrent-migrate -i --data /torrents /nas/torrents .rtorrent

* verbose output. print every change made::

    rtorrent-migrate -v --data /torrents /nas/torrents .rtorrent

* dry run. print the changes that would have been made, but do not write them to disk::

    rtorrent-migrate -vn --data /torrents /nas/torrents .rtorrent
