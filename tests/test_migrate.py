import io
import os
import unittest.mock

from rtorrent_migrate import RTorrentMigrate

from .base import _TestRTorrentBase


class TestRTorrentMigrate(_TestRTorrentBase):
    def test_migrate_data(self) -> None:
        """Migrate the data dir in place"""
        migrator = RTorrentMigrate(data_old='/OLD/DATA', data_new='/NEW-DATA')
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')

        migrator.migrate(file)

        with open(file, 'rb') as fh:
            new_contents = fh.read()

        self.assertEqual(new_contents, self._contents['data_changed'])

    def test_migrate_session(self) -> None:
        """Migrate the session dir in place"""
        migrator = RTorrentMigrate(
            session_old='/OLD/SESSION', session_new='/NEW-SESSION'
        )
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')

        migrator.migrate(file)

        with open(file, 'rb') as fh:
            new_contents = fh.read()

        self.assertEqual(new_contents, self._contents['session_changed'])

    def test_migrate_both(self) -> None:
        """Migrate both the data and session dir"""
        migrator = RTorrentMigrate(
            data_old='/OLD/DATA', data_new='/NEW-DATA',
            session_old='/OLD/SESSION', session_new='/NEW-SESSION'
        )
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')

        migrator.migrate(file)

        with open(file, 'rb') as fh:
            new_contents = fh.read()

        self.assertEqual(new_contents, self._contents['both_changed'])

    def test_migrate_newfile(self) -> None:
        """Migrate to a new file"""
        migrator = RTorrentMigrate(data_old='/OLD/DATA', data_new='/NEW-DATA')
        old_file = os.path.join(self.testdir, 'basic.torrent.rtorrent')
        new_file = os.path.join(self.testdir, 'new')

        migrator.migrate(old_file, new_file)

        with open(old_file, 'rb') as fh:
            old_contents = fh.read()
        with open(new_file, 'rb') as fh:
            new_contents = fh.read()

        # verify the original file wasn't changed
        self.assertEqual(old_contents, self._contents['unchanged'])
        self.assertEqual(new_contents, self._contents['data_changed'])

    def test_migrate_only_data_changed(self) -> None:
        """Migrate both the data and session dir, but only data should
        change"""
        migrator = RTorrentMigrate(
            data_old='/OLD/DATA', data_new='/NEW-DATA',
            session_old='DIFFERENT', session_new='/NEW-SESSION'
        )
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')

        migrator.migrate(file)

        with open(file, 'rb') as fh:
            new_contents = fh.read()

        self.assertEqual(new_contents, self._contents['data_changed'])

    def test_migrate_only_session_changed(self) -> None:
        """Migrate both the data and session dir, but only session
        should change"""
        migrator = RTorrentMigrate(
            data_old='DIFFERENT', data_new='/NEW-DATA',
            session_old='/OLD/SESSION', session_new='/NEW-SESSION'
        )
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')

        migrator.migrate(file)

        with open(file, 'rb') as fh:
            new_contents = fh.read()

        self.assertEqual(new_contents, self._contents['session_changed'])

    def test_migrate_no_change(self) -> None:
        """Migrate when no changes are needed. File should not change"""
        migrator = RTorrentMigrate(
            data_old='DIFFERENT', data_new='/NEW-DATA',
            session_old='DIFFERENT', session_new='/NEW-SESSION'
        )
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')
        mtime_before = os.path.getmtime(file)

        migrator.migrate(file)

        mtime_after = os.path.getmtime(file)
        with open(file, 'rb') as fh:
            new_contents = fh.read()

        self.assertEqual(new_contents, self._contents['unchanged'])
        # verify that the modification time hasn't changed. this is
        # done to verify that the file wasn't rewritten
        self.assertEqual(mtime_before, mtime_after)

    def test_migrate_slash(self) -> None:
        """data dir has trailing slash"""
        migrator = RTorrentMigrate(data_old='/OLD/DATA/', data_new='/NEW-DATA')
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')

        migrator.migrate(file)

        with open(file, 'rb') as fh:
            new_contents = fh.read()

        self.assertEqual(new_contents, self._contents['data_changed'])

    def test_migrate_partial_dir(self) -> None:
        """Don't match partial dir"""
        migrator = RTorrentMigrate(data_old='/OLD/DAT', data_new='/NEW-DATA')
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')

        migrator.migrate(file)

        with open(file, 'rb') as fh:
            new_contents = fh.read()

        self.assertEqual(new_contents, self._contents['unchanged'])

    def test_migrate_bytes(self) -> None:
        """Migrate using byte strings"""
        migrator = RTorrentMigrate(
            data_old=b'/OLD/DATA', data_new=b'/NEW-DATA',
            session_old=b'/OLD/SESSION', session_new=b'/NEW-SESSION'
        )
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')

        migrator.migrate(file)

        with open(file, 'rb') as fh:
            new_contents = fh.read()

        self.assertEqual(new_contents, self._contents['both_changed'])

    def test_migrate_dry_run(self) -> None:
        """Dry run test"""
        migrator = RTorrentMigrate(
            data_old='/OLD/DATA', data_new='/NEW-DATA',
            session_old='/OLD/SESSION', session_new='/NEW-SESSION',
            dry_run=True
        )
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')
        mtime_before = os.path.getmtime(file)

        migrator.migrate(file)

        mtime_after = os.path.getmtime(file)
        with open(file, 'rb') as fh:
            new_contents = fh.read()

        self.assertEqual(new_contents, self._contents['unchanged'])
        # verify that the modification time hasn't changed
        self.assertEqual(mtime_before, mtime_after)

    # redirect stdout to the var `stdout`
    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_migrate_verbose(self, stdout: io.StringIO) -> None:
        """Verbose test

        Should produce output to stdout

        The contents of the output isn't checked
        """
        migrator = RTorrentMigrate(
            data_old='/OLD/DATA', data_new='/NEW-DATA',
            session_old='/OLD/SESSION', session_new='/NEW-SESSION',
            verbose=True
        )
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')

        migrator.migrate(file)
        stdout_value = stdout.getvalue().strip()

        self.assertNotEqual(stdout_value, '')

    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_migrate_not_verbose(self, stdout: io.StringIO) -> None:
        """Not verbose test

        Should not produce any output
        """
        migrator = RTorrentMigrate(
            data_old='/OLD/DATA', data_new='/NEW-DATA',
            session_old='/OLD/SESSION', session_new='/NEW-SESSION',
            verbose=False
        )
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')

        migrator.migrate(file)
        stdout_value = stdout.getvalue().strip()

        self.assertEqual(stdout_value, '')

    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_migrate_verbose_default(self, stdout: io.StringIO) -> None:
        """Default verbose test

        Verbose defaults to `False`, so this should be equivalent to
        ``test_migrate_not_verbose``
        """
        migrator = RTorrentMigrate(
            data_old='/OLD/DATA', data_new='/NEW-DATA',
            session_old='/OLD/SESSION', session_new='/NEW-SESSION'
        )
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')

        migrator.migrate(file)
        stdout_value = stdout.getvalue().strip()

        self.assertEqual(stdout_value, '')

    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_migrate_verbose_unicode_error(self, stdout: io.StringIO) -> None:
        """Verbose output when the data and session paths aren't
        utf8-encoded

        Shouldn't raise an exception. The paths will be left as bytes
        """
        migrator = RTorrentMigrate(
            data_old='/OLD/DATA', data_new='/NEW-DATA',
            session_old='/OLD/SESSION', session_new='/NEW-SESSION',
            verbose=True
        )
        file = os.path.join(self.testdir, 'invalid-unicode')

        migrator.migrate(file)
        stdout_value = stdout.getvalue().strip()

        self.assertNotEqual(stdout_value, '')
