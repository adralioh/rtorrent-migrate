from rtorrent_migrate import RTorrentMigrate

from .base import _TestRTorrentBase


class TestRTorrentConstructor(_TestRTorrentBase):
    def test_no_args(self) -> None:
        """Constructor should raise ValueError if neither the data nor
        sessions kwargs are given
        """
        self.assertRaises(ValueError, RTorrentMigrate)

    def test_only_data_old(self) -> None:
        """Constructor should raise ValueError if data_old is given
        without data_new
        """
        self.assertRaises(ValueError, RTorrentMigrate, data_old='value')

    def test_only_data_new(self) -> None:
        """Constructor should raise ValueError if data_new is given
        without data_old
        """
        self.assertRaises(ValueError, RTorrentMigrate, data_new='value')

    def test_only_session_old(self) -> None:
        """Constructor should raise ValueError if session_old is given
        without session_new
        """
        self.assertRaises(ValueError, RTorrentMigrate, session_old='value')

    def test_only_session_new(self) -> None:
        """Constructor should raise ValueError if session_new is given
        without session_old
        """
        self.assertRaises(ValueError, RTorrentMigrate, session_new='value')

    def test_both_data(self) -> None:
        """Constructor should not raise an exception if both data kwargs
        are given
        """
        RTorrentMigrate(data_old='value', data_new='value')

    def test_both_session(self) -> None:
        """Constructor should not raise an exception if both session
        kwargs are given
        """
        RTorrentMigrate(session_old='value', session_new='value')
