import io
import os
import unittest.mock

from rtorrent_migrate import RTorrentMigrate

from .base import _TestRTorrentBase


class TestRTorrentMigrateDir(_TestRTorrentBase):
    def test_migrate_dir(self) -> None:
        """Basic ``migrate_dir`` test

        :basic.torrent.rtorrent: should change
        :no-change.torrent.rtorrent:
            shouldn't change because its data dir doesn't match
        :different.file.extension:
            shouldn't change because the filename doesn't match
            `file_regex`
        """
        migrator = RTorrentMigrate(data_old='/OLD/DATA', data_new='/NEW-DATA')
        dir_ = os.path.join(self.testdir, 'dir')
        mtime_before = {}
        mtime_after = {}
        new_contents = {}
        files = (
            'basic.torrent.rtorrent', 'no-change.torrent.rtorrent',
            'different.file.extension'
        )

        for file in files:
            path = os.path.join(dir_, file)
            mtime_before[file] = os.path.getmtime(path)

        migrator.migrate_dir(dir_)

        for file in files:
            path = os.path.join(dir_, file)

            mtime_after[file] = os.path.getmtime(path)
            with open(path, 'rb') as fh:
                new_contents[file] = fh.read()

        self.assertEqual(
            new_contents['basic.torrent.rtorrent'],
            self._contents['data_changed']
        )
        self.assertEqual(
            new_contents['no-change.torrent.rtorrent'],
            self._contents['no-change']
        )
        self.assertEqual(
            new_contents['different.file.extension'],
            self._contents['unchanged']
        )

        self.assertEqual(
            mtime_before['no-change.torrent.rtorrent'],
            mtime_after['no-change.torrent.rtorrent']
        )
        self.assertEqual(
            mtime_before['different.file.extension'],
            mtime_after['different.file.extension']
        )

    def test_migrate_dir_regex_constructor(self) -> None:
        """Different file regex specified via the constructor

        :basic.torrent.rtorrent: shouldn't change
        :no-change.torrent.rtorrent: should't change
        :different.file.extension: should change
        :partial.extension.match:
            shouldn't change because the regex only partially matches
        """
        migrator = RTorrentMigrate(
            data_old='/OLD/DATA', data_new='/NEW-DATA',
            file_regex=r'.+\.extension'
        )
        dir_ = os.path.join(self.testdir, 'dir')
        mtime_before = {}
        mtime_after = {}
        new_contents = {}
        files = (
            'basic.torrent.rtorrent', 'no-change.torrent.rtorrent',
            'different.file.extension', 'partial.extension.match'
        )

        for file in files:
            path = os.path.join(dir_, file)
            mtime_before[file] = os.path.getmtime(path)

        migrator.migrate_dir(dir_)

        for file in files:
            path = os.path.join(dir_, file)

            mtime_after[file] = os.path.getmtime(path)
            with open(path, 'rb') as fh:
                new_contents[file] = fh.read()

        self.assertEqual(
            new_contents['basic.torrent.rtorrent'], self._contents['unchanged']
        )
        self.assertEqual(
            new_contents['no-change.torrent.rtorrent'],
            self._contents['no-change']
        )
        self.assertEqual(
            new_contents['different.file.extension'],
            self._contents['data_changed']
        )
        self.assertEqual(
            new_contents['partial.extension.match'],
            self._contents['unchanged']
        )

        self.assertEqual(
            mtime_before['basic.torrent.rtorrent'],
            mtime_after['basic.torrent.rtorrent']
        )
        self.assertEqual(
            mtime_before['no-change.torrent.rtorrent'],
            mtime_after['no-change.torrent.rtorrent']
        )
        self.assertEqual(
            mtime_before['partial.extension.match'],
            mtime_after['partial.extension.match']
        )

    def test_migrate_dir_regex_function(self) -> None:
        """Different file regex specified via argument to
        ``migrate_dir``

        :basic.torrent.rtorrent: shouldn't change
        :no-change.torrent.rtorrent: shouldn't change
        :different.file.extension: should change
        :partial.extension.match:
            shouldn't change because the regex only partially matches
        """
        migrator = RTorrentMigrate(data_old='/OLD/DATA', data_new='/NEW-DATA')
        dir_ = os.path.join(self.testdir, 'dir')
        mtime_before = {}
        mtime_after = {}
        new_contents = {}
        files = (
            'basic.torrent.rtorrent', 'no-change.torrent.rtorrent',
            'different.file.extension', 'partial.extension.match'
        )

        for file in files:
            path = os.path.join(dir_, file)
            mtime_before[file] = os.path.getmtime(path)

        migrator.migrate_dir(dir_, file_regex=r'.+\.extension')

        for file in files:
            path = os.path.join(dir_, file)

            mtime_after[file] = os.path.getmtime(path)
            with open(path, 'rb') as fh:
                new_contents[file] = fh.read()

        self.assertEqual(
            new_contents['basic.torrent.rtorrent'], self._contents['unchanged']
        )
        self.assertEqual(
            new_contents['no-change.torrent.rtorrent'],
            self._contents['no-change']
        )
        self.assertEqual(
            new_contents['different.file.extension'],
            self._contents['data_changed']
        )
        self.assertEqual(
            new_contents['partial.extension.match'],
            self._contents['unchanged']
        )

        self.assertEqual(
            mtime_before['basic.torrent.rtorrent'],
            mtime_after['basic.torrent.rtorrent']
        )
        self.assertEqual(
            mtime_before['no-change.torrent.rtorrent'],
            mtime_after['no-change.torrent.rtorrent']
        )
        self.assertEqual(
            mtime_before['partial.extension.match'],
            mtime_after['partial.extension.match']
        )

    def test_migrate_dir_recursive(self) -> None:
        """Nested dirs

        Both files should change
        """
        migrator = RTorrentMigrate(data_old='/OLD/DATA', data_new='/NEW-DATA')
        dir_ = os.path.join(self.testdir, 'recursive')
        new_contents = {}
        files = {
            '1': os.path.join(dir_, '1', '1.torrent.rtorrent'),
            '2': os.path.join(dir_, '1', '2', '2.torrent.rtorrent')
        }

        migrator.migrate_dir(dir_)

        for name, path in files.items():
            with open(path, 'rb') as fh:
                new_contents[name] = fh.read()

        # 1/1.torrent.rtorrent
        self.assertEqual(
            new_contents['1'],
            self._contents['data_changed']
        )
        # 1/2/2.torrent.rtorrent
        self.assertEqual(
            new_contents['2'],
            self._contents['data_changed']
        )

    def test_migrate_dir_errors(self) -> None:
        """Raise an exception when ``ignore_errors`` is `False`"""
        migrator = RTorrentMigrate(data_old='/OLD/DATA', data_new='/NEW-DATA')
        dir_ = os.path.join(self.testdir, 'errors')

        self.assertRaises(Exception, migrator.migrate_dir, dir_)

    def test_migrate_dir_ignore_errors(self) -> None:
        """Ignore errors

        :basic.torrent.rtorrent: should change
        :missing_key.torrent.rtorrent: an empty dict
        :not_bencode.torrent.rtorrent: not valid bencode
        """
        migrator = RTorrentMigrate(data_old='/OLD/DATA', data_new='/NEW-DATA')
        dir_ = os.path.join(self.testdir, 'errors')
        mtime_before = {}
        mtime_after = {}
        new_contents = {}
        files = (
            'basic.torrent.rtorrent', 'missing_key.torrent.rtorrent',
            'not_bencode.torrent.rtorrent'
        )

        for file in files:
            path = os.path.join(dir_, file)
            mtime_before[file] = os.path.getmtime(path)

        self.assertWarns(
            UserWarning, migrator.migrate_dir, dir_, ignore_errors=True
        )

        for file in files:
            path = os.path.join(dir_, file)

            mtime_after[file] = os.path.getmtime(path)
            with open(path, 'rb') as fh:
                new_contents[file] = fh.read()

        self.assertEqual(
            new_contents['basic.torrent.rtorrent'],
            self._contents['data_changed']
        )
        self.assertEqual(new_contents['missing_key.torrent.rtorrent'], b'de')
        self.assertEqual(
            new_contents['not_bencode.torrent.rtorrent'], b'NOT_BENCODE\n'
        )

        self.assertEqual(
            mtime_before['missing_key.torrent.rtorrent'],
            mtime_after['missing_key.torrent.rtorrent']
        )
        self.assertEqual(
            mtime_before['not_bencode.torrent.rtorrent'],
            mtime_after['not_bencode.torrent.rtorrent']
        )

    def test_migrate_dir_walk_errors(self) -> None:
        """Raise an exception when os.walk raises an error when
        ``ignore_errors`` is `False`

        `FAKE_DIR` doesn't exist, so should raise `FileNotFoundError`
        """
        migrator = RTorrentMigrate(data_old='/OLD/DATA', data_new='/NEW-DATA')
        dir_ = os.path.join(self.testdir, 'FAKE_DIR')

        # ignore_errors should default to False
        self.assertRaises(FileNotFoundError, migrator.migrate_dir, dir_)
        # explicitly set ignore_errors to False
        self.assertRaises(
            FileNotFoundError, migrator.migrate_dir, dir_, ignore_errors=False
        )

    def test_migrate_dir_ignore_walk_errors(self) -> None:
        """Ignore errors risen by os.walk

        `FAKE_DIR` doesn't exist, so will raise `FileNotFoundError`
        """
        migrator = RTorrentMigrate(data_old='/OLD/DATA', data_new='/NEW-DATA')
        dir_ = os.path.join(self.testdir, 'FAKE_DIR')

        self.assertWarns(
            UserWarning, migrator.migrate_dir, dir_, ignore_errors=True
        )

    def test_migrate_dir_regex_recursive(self) -> None:
        """Verify that ``file_regex`` only matches the filename, not the
        dir name

        :dir/exact_match: should change
        """
        migrator = RTorrentMigrate(data_old='/OLD/DATA', data_new='/NEW-DATA')
        dir_ = os.path.join(self.testdir, 'regex_recursive')
        file = os.path.join(dir_, 'dir', 'exact_match')

        migrator.migrate_dir(dir_, file_regex='exact_match')

        with open(file, 'rb') as fh:
            new_contents = fh.read()

        self.assertEqual(new_contents, self._contents['data_changed'])

    # redirect stdout to the var `stdout`
    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_migrate_dir_verbose(self, stdout: io.StringIO) -> None:
        """Verbose dir test

        `empty` contains a single file that doesn't match the default
        regex, so this won't call ``migrate``. only ``migrate_dir`` is
        checked for verbosity

        Should produce output to stdout

        The contents of the output isn't checked
        """
        migrator = RTorrentMigrate(
            data_old='/OLD/DATA', data_new='/NEW-DATA',
            session_old='/OLD/SESSION', session_new='/NEW-SESSION',
            verbose=True
        )
        dir_ = os.path.join(self.testdir, 'empty')

        migrator.migrate_dir(dir_)
        stdout_value = stdout.getvalue().strip()

        self.assertNotEqual(stdout_value, '')

    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_migrate_dir_not_verbose(self, stdout: io.StringIO) -> None:
        """Not verbose dir test

        Should not produce any output
        """
        migrator = RTorrentMigrate(
            data_old='/OLD/DATA', data_new='/NEW-DATA',
            session_old='/OLD/SESSION', session_new='/NEW-SESSION',
            verbose=False
        )
        dir_ = os.path.join(self.testdir, 'empty')

        migrator.migrate_dir(dir_)
        stdout_value = stdout.getvalue().strip()

        self.assertEqual(stdout_value, '')

    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_migrate_dir_verbose_default(self, stdout: io.StringIO) -> None:
        """Default verbose dir test

        Verbose defaults to `False`, so this should be equivalent to
        ``test_migrate_dir_not_verbose``
        """
        migrator = RTorrentMigrate(
            data_old='/OLD/DATA', data_new='/NEW-DATA',
            session_old='/OLD/SESSION', session_new='/NEW-SESSION'
        )
        dir_ = os.path.join(self.testdir, 'empty')

        migrator.migrate_dir(dir_)
        stdout_value = stdout.getvalue().strip()

        self.assertEqual(stdout_value, '')
