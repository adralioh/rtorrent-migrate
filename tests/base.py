import os
import shutil
import tempfile
import unittest


class _TestRTorrentBase(unittest.TestCase):
    """Base test class used as the parent of the other test classes"""

    _file_dir = os.path.join(
        os.path.dirname(__file__), 'files'
    )
    """The path to the dir that contains files used for testing

    These files are copied to a temp dir before each test is run, where
    they can be freely modified

    The path to the tempdir is saved to ``testdir``
    """

    _contents = {
        'unchanged': (
            b'd9:directory22:/OLD/DATA/sub/contents'
            b'11:loaded_file28:/OLD/SESSION/torrent.torrente'
        ),
        'data_changed': (
            b'd9:directory22:/NEW-DATA/sub/contents'
            b'11:loaded_file28:/OLD/SESSION/torrent.torrente'
        ),
        'session_changed': (
            b'd9:directory22:/OLD/DATA/sub/contents'
            b'11:loaded_file28:/NEW-SESSION/torrent.torrente'
        ),
        'both_changed': (
            b'd9:directory22:/NEW-DATA/sub/contents'
            b'11:loaded_file28:/NEW-SESSION/torrent.torrente'
        ),
        'no-change': (
            b'd9:directory23:/DIFFERENT/sub/contents'
            b'11:loaded_file26:/DIFFERENT/torrent.torrente'
        )
    }
    """dict of file contents of files in ``_file_dir``

    :unchanged:
        the default contents of most files, including
        `basic.torrent.rtorrent`
    :data_changed:
        same as `unchanged`, but with the data dir changed from
        `/OLD/DATA` to `/NEW-DATA`
    :session_changed:
        same as `unchanged`, but with the session dir changed from
        `/OLD/session` to `/NEW-session`
    :both_changed:
        same as `unchanged`, but with both the data and session dirs
        changed
    :no-change: the default contents of `dir/no-change.torrent.rtorrent`
    """

    def setUp(self) -> None:
        """Create a temp dir that contains a copy of all files in
        ``_file_dir``

        The path to the tempdir is saved to ``testdir``
        """
        self._tempdir = tempfile.TemporaryDirectory(suffix='.rtorrent-migrate')
        # use a subdir within the temp dir because shutil.copytree fails
        # if the parent dir already exists
        #
        # the `dirs_exist_ok` arg was added in Python 3.8 which avoids
        # this issue, but we want to maintain compatibility with older
        # versions
        self.testdir = os.path.join(self._tempdir.name, 'files')

        shutil.copytree(self._file_dir, self.testdir)

    def tearDown(self) -> None:
        self._tempdir.cleanup()
