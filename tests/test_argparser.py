import unittest

from rtorrent_migrate.migrator import _get_parser


class TestRTorrentArgParser(unittest.TestCase):
    """Verify that the ``include_epilog`` arg of
    ``rtorrent_migrate._get_parser`` works
    """
    def test_get_parser_default(self) -> None:
        """The epilog should be `None` when no args are given"""
        parser = _get_parser()
        self.assertIsNone(parser.epilog)

    def test_get_parser_epilog(self) -> None:
        """The epilog should not be `None` when ``include_epilog`` is
        `True`
        """
        parser = _get_parser(True)
        self.assertIsNotNone(parser.epilog)
