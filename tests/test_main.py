import io
import os
from typing import Any
import unittest.mock

from rtorrent_migrate.migrator import _main as rt_main

from .base import _TestRTorrentBase


class TestRTorrentMigrateMain(_TestRTorrentBase):
    """Basic tests for rtorrent_migrate.__main__

    Comprehensive testing is done in the other test classes that create
    the RTorrentMigrate class directly
    """

    prog = 'rtorrent-migrate'
    """the value that `sys.argv[0]` is set to when calling
    rtorrent_migrate.__main__
    """

    # redirect stderr to a dummy object
    # equivalent to `2> /dev/null`
    @unittest.mock.patch('sys.stderr')
    def test_no_args(self, _: Any) -> None:
        """Raise error when no args are given"""
        argv = [self.prog]

        # override the arguments passed to argparse when calling main
        with unittest.mock.patch('sys.argv', new=argv):
            self.assertRaises(SystemExit, rt_main)

    @unittest.mock.patch('sys.stderr')
    def test_unrecognized_arg(self, _: Any) -> None:
        """Raise error when an invalid arg is given"""
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')
        # also pass a file and --data so that it doesn't raise an
        # unrelated error
        argv = [self.prog, file, '-d', '/D1', '/D2', '--FAKEARG']

        with unittest.mock.patch('sys.argv', new=argv):
            self.assertRaises(SystemExit, rt_main)

    @unittest.mock.patch('sys.stderr')
    def test_only_positional(self, _: Any) -> None:
        """Raise error when neither --data nor --session are given"""
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')
        argv = [self.prog, file]

        with unittest.mock.patch('sys.argv', new=argv):
            self.assertRaises(SystemExit, rt_main)

    def test_data(self) -> None:
        """Migrate data dir on a single file"""
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')
        argv = [self.prog, file, '--data', '/OLD/DATA', '/NEW-DATA']

        with unittest.mock.patch('sys.argv', new=argv):
            rt_main()

        with open(file, 'rb') as fh:
            new_contents = fh.read()

        self.assertEqual(new_contents, self._contents['data_changed'])

    def test_session(self) -> None:
        """Migrate session dir on a single file"""
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')
        argv = [self.prog, file, '--session', '/OLD/SESSION', '/NEW-SESSION']

        with unittest.mock.patch('sys.argv', new=argv):
            rt_main()

        with open(file, 'rb') as fh:
            new_contents = fh.read()

        self.assertEqual(new_contents, self._contents['session_changed'])

    def test_dir(self) -> None:
        """Migrate dir

        This is the same as test as
        `TestRTorrentMigrateDir.test_migrate_dir`, except it runs via
        `rtorrent_migrate.__main__` instead of calling the class
        directly

        :basic.torrent.rtorrent: should change
        :no-change.torrent.rtorrent:
            shouldn't change because its data dir doesn't match
        :different.file.extension:
            shouldn't change because the filename doesn't match
            `file_regex`
        """
        dir_ = os.path.join(self.testdir, 'dir')
        argv = [self.prog, dir_, '-d', '/OLD/DATA', '/NEW-DATA']

        mtime_before = {}
        mtime_after = {}
        new_contents = {}
        files = (
            'basic.torrent.rtorrent', 'no-change.torrent.rtorrent',
            'different.file.extension'
        )

        for file in files:
            path = os.path.join(dir_, file)
            mtime_before[file] = os.path.getmtime(path)

        with unittest.mock.patch('sys.argv', new=argv):
            rt_main()

        for file in files:
            path = os.path.join(dir_, file)

            mtime_after[file] = os.path.getmtime(path)
            with open(path, 'rb') as fh:
                new_contents[file] = fh.read()

        self.assertEqual(
            new_contents['basic.torrent.rtorrent'],
            self._contents['data_changed']
        )
        self.assertEqual(
            new_contents['no-change.torrent.rtorrent'],
            self._contents['no-change']
        )
        self.assertEqual(
            new_contents['different.file.extension'],
            self._contents['unchanged']
        )

        self.assertEqual(
            mtime_before['no-change.torrent.rtorrent'],
            mtime_after['no-change.torrent.rtorrent']
        )
        self.assertEqual(
            mtime_before['different.file.extension'],
            mtime_after['different.file.extension']
        )

    def test_combined(self) -> None:
        """Migrate multiple files and directories

        :basic.torrent.rtorrent:
            should change because it's given directly
        :dir/basic.torrent.rtorrent:
            shouldn't change because it isn't given
        :dir/different.file.extension:
            should change because it's given directly
        :dir/partial.extension.match:
            shouldn't change because it isn't given
        :recursive/1/1.torrent.rtorrent:
            should change because it's within a dir that is given
        :recursive/1/2/2.torrent.rtorrent:
            should change because it's within a dir that is given
        """
        dir_ = self.testdir
        argv = [
            self.prog, '-s', '/OLD/SESSION', '/NEW-SESSION',
            os.path.join(dir_, 'basic.torrent.rtorrent'),
            os.path.join(dir_, 'dir', 'different.file.extension'),
            os.path.join(dir_, 'recursive')
        ]

        new_contents = {}
        files = {
            'basic': os.path.join(dir_, 'basic.torrent.rtorrent',),
            'dir/basic': os.path.join(dir_, 'dir', 'basic.torrent.rtorrent'),
            'dir/different': os.path.join(
                dir_, 'dir', 'different.file.extension'
            ),
            'dir/partial': os.path.join(
                dir_, 'dir', 'partial.extension.match'
            ),
            'recursive/1': os.path.join(
                dir_, 'recursive', '1', '1.torrent.rtorrent'
            ),
            'recursive/2': os.path.join(
                dir_, 'recursive', '1', '2', '2.torrent.rtorrent'
            )
        }

        with unittest.mock.patch('sys.argv', new=argv):
            rt_main()

        for name, path in files.items():
            with open(path, 'rb') as fh:
                new_contents[name] = fh.read()

        # basic.torrent.rtorrent
        self.assertEqual(
            new_contents['basic'], self._contents['session_changed']
        )
        # dir/basic.torrent.rtorrent
        self.assertEqual(
            new_contents['dir/basic'], self._contents['unchanged']
        )
        # dir/different.file.extension
        self.assertEqual(
            new_contents['dir/different'], self._contents['session_changed']
        )
        # dir/partial.extension.match
        self.assertEqual(
            new_contents['dir/partial'], self._contents['unchanged']
        )
        # recursive/1/1.torrent.rtorrent
        self.assertEqual(
            new_contents['recursive/1'], self._contents['session_changed']
        )
        # recursive/1/2/2.torrent.rtorrent
        self.assertEqual(
            new_contents['recursive/2'], self._contents['session_changed']
        )

    def test_regex(self) -> None:
        """Regex argument

        This is the same test as
        `TestRTorrentMigrateDir.test_migrate_dir_regex_constructor`

        :basic.torrent.rtorrent: shouldn't change
        :no-change.torrent.rtorrent: should't change
        :different.file.extension: should change
        :partial.extension.match:
            shouldn't change because the regex only partially matches
        """
        dir_ = os.path.join(self.testdir, 'dir')
        argv = [
            self.prog, dir_, '-d', '/OLD/DATA', '/NEW-DATA',
            '--regex', r'.+\.extension',
        ]
        mtime_before = {}
        mtime_after = {}
        new_contents = {}
        files = (
            'basic.torrent.rtorrent', 'no-change.torrent.rtorrent',
            'different.file.extension', 'partial.extension.match'
        )

        for file in files:
            path = os.path.join(dir_, file)
            mtime_before[file] = os.path.getmtime(path)

        with unittest.mock.patch('sys.argv', new=argv):
            rt_main()

        for file in files:
            path = os.path.join(dir_, file)

            mtime_after[file] = os.path.getmtime(path)
            with open(path, 'rb') as fh:
                new_contents[file] = fh.read()

        self.assertEqual(
            new_contents['basic.torrent.rtorrent'], self._contents['unchanged']
        )
        self.assertEqual(
            new_contents['no-change.torrent.rtorrent'],
            self._contents['no-change']
        )
        self.assertEqual(
            new_contents['different.file.extension'],
            self._contents['data_changed']
        )
        self.assertEqual(
            new_contents['partial.extension.match'],
            self._contents['unchanged']
        )

        self.assertEqual(
            mtime_before['basic.torrent.rtorrent'],
            mtime_after['basic.torrent.rtorrent']
        )
        self.assertEqual(
            mtime_before['no-change.torrent.rtorrent'],
            mtime_after['no-change.torrent.rtorrent']
        )
        self.assertEqual(
            mtime_before['partial.extension.match'],
            mtime_after['partial.extension.match']
        )

    def test_errors_dir(self) -> None:
        """Raise an exception when an error is encountered within a
        directory and `ignore` isn't given

        Same test as `TestRTorrentMigrateDir.test_migrate_dir_errors`
        """
        dir_ = os.path.join(self.testdir, 'errors')
        argv = [self.prog, dir_, '-d', '/OLD/DATA', '/NEW-DATA']

        with unittest.mock.patch('sys.argv', new=argv):
            self.assertRaises(Exception, rt_main)

    def test_ignore_errors_dir(self) -> None:
        """Ignore errors raised within directories

        This is the same test as
        `TestRTorrentMigrateDir.test_migrate_dir_ignore_errors`

        :basic.torrent.rtorrent: should change
        :missing_key.torrent.rtorrent: an empty dict
        :not_bencode.torrent.rtorrent: not valid bencode
        """
        dir_ = os.path.join(self.testdir, 'errors')
        argv = [
            self.prog, dir_, '--ignore', '-d', '/OLD/DATA', '/NEW-DATA',
        ]
        mtime_before = {}
        mtime_after = {}
        new_contents = {}
        files = (
            'basic.torrent.rtorrent', 'missing_key.torrent.rtorrent',
            'not_bencode.torrent.rtorrent'
        )

        for file in files:
            path = os.path.join(dir_, file)
            mtime_before[file] = os.path.getmtime(path)

        with unittest.mock.patch('sys.argv', new=argv):
            self.assertWarns(UserWarning, rt_main)

        for file in files:
            path = os.path.join(dir_, file)

            mtime_after[file] = os.path.getmtime(path)
            with open(path, 'rb') as fh:
                new_contents[file] = fh.read()

        self.assertEqual(
            new_contents['basic.torrent.rtorrent'],
            self._contents['data_changed']
        )
        self.assertEqual(new_contents['missing_key.torrent.rtorrent'], b'de')
        self.assertEqual(
            new_contents['not_bencode.torrent.rtorrent'], b'NOT_BENCODE\n'
        )

        self.assertEqual(
            mtime_before['missing_key.torrent.rtorrent'],
            mtime_after['missing_key.torrent.rtorrent']
        )
        self.assertEqual(
            mtime_before['not_bencode.torrent.rtorrent'],
            mtime_after['not_bencode.torrent.rtorrent']
        )

    def test_errors_file(self) -> None:
        """Raise an exception when an error is encountered within a
        single file and `ignore` isn't given
        """
        file = os.path.join(
            self.testdir, 'errors', 'missing_key.torrent.rtorrent'
        )
        argv = [self.prog, file, '-d', '/OLD/DATA', '/NEW-DATA']

        with unittest.mock.patch('sys.argv', new=argv):
            self.assertRaises(Exception, rt_main)

    def test_ignore_errors_file(self) -> None:
        """Ignore errors within a single file"""
        dir_ = self.testdir
        files = {
            'error': os.path.join(
                dir_, 'errors', 'missing_key.torrent.rtorrent'
            ),
            'basic': os.path.join(dir_, 'basic.torrent.rtorrent')
        }
        argv = [
            self.prog, *files.values(),
            '--ignore', '-d', '/OLD/DATA', '/NEW-DATA'
        ]
        mtime_before = {}
        mtime_after = {}
        new_contents = {}

        for name, path in files.items():
            mtime_before[name] = os.path.getmtime(path)

        with unittest.mock.patch('sys.argv', new=argv):
            self.assertWarns(UserWarning, rt_main)

        for name, path in files.items():
            mtime_after[name] = os.path.getmtime(path)
            with open(path, 'rb') as fh:
                new_contents[name] = fh.read()

        self.assertEqual(new_contents['basic'], self._contents['data_changed'])
        self.assertEqual(new_contents['error'], b'de')

        self.assertEqual(mtime_before['error'], mtime_after['error'])

    def test_missing_file(self) -> None:
        """Raise an exception when a given file doesn't exist"""
        file = os.path.join(self.testdir, 'not/a/file')
        argv = [self.prog, file, '-d', '/OLD/DATA', '/NEW-DATA']

        with unittest.mock.patch('sys.argv', new=argv):
            self.assertRaises(FileNotFoundError, rt_main)

    def test_ignore_missing_file(self) -> None:
        """Ignore missing file"""
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')
        missing = os.path.join(self.testdir, 'not/a/file')
        argv = [
            self.prog, file, missing,
            '--ignore', '-d', '/OLD/DATA', '/NEW-DATA'
        ]

        with unittest.mock.patch('sys.argv', new=argv):
            self.assertWarns(UserWarning, rt_main)

        with open(file, 'rb') as fh:
            new_content = fh.read()

        self.assertEqual(new_content, self._contents['data_changed'])

    def test_dry_run(self) -> None:
        """Dry run arg

        Same test as `TestRTorrentMigrate.test_migrate_dry_run`
        """
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')
        argv = [
            self.prog, file, '--dry-run',
            '-d', '/OLD/DATA', '/NEW-DATA',
            '-s', '/OLD/SESSION', '/NEW-SESSION',
        ]
        mtime_before = os.path.getmtime(file)

        with unittest.mock.patch('sys.argv', new=argv):
            rt_main()

        mtime_after = os.path.getmtime(file)
        with open(file, 'rb') as fh:
            new_contents = fh.read()

        self.assertEqual(new_contents, self._contents['unchanged'])
        self.assertEqual(mtime_before, mtime_after)

    # redirect stdout to the var `stdout`
    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_migrate_verbose(self, stdout: io.StringIO) -> None:
        """Verbose arg

        Same test as `TestRTorrentMigrate.test_migrate_verbose`

        Should produce output to stdout

        The contents of the output isn't checked
        """
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')
        argv = [
            self.prog, file, '--verbose',
            '-d', '/OLD/DATA', '/NEW-DATA',
            '-s', '/OLD/SESSION', '/NEW-SESSION',
        ]

        with unittest.mock.patch('sys.argv', new=argv):
            rt_main()

        stdout_value = stdout.getvalue().strip()

        self.assertNotEqual(stdout_value, '')

    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_migrate_not_verbose(self, stdout: io.StringIO) -> None:
        """Verbose arg not given

        Same test as `TestRTorrentMigrate.test_migrate_not_verbose`

        Should not produce any output
        """
        file = os.path.join(self.testdir, 'basic.torrent.rtorrent')
        argv = [
            self.prog, file,
            '-d', '/OLD/DATA', '/NEW-DATA',
            '-s', '/OLD/SESSION', '/NEW-SESSION',
        ]

        with unittest.mock.patch('sys.argv', new=argv):
            rt_main()

        stdout_value = stdout.getvalue().strip()

        self.assertEqual(stdout_value, '')
